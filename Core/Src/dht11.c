/*
 * dht11.c
 *
 *  Created on: Dec 8, 2020
 *      Author: Andrea
 *
 *  DHT11
 */

#include "stm32f4xx_hal.h"
#include "general.h"
#include "DHT11.h"

uint8_t Rh_byte1, Rh_byte2, Temp_byte1, Temp_byte2;
uint16_t SUM; uint8_t Presence = 0;

void DHT11_Start (void) //inicializa
{
	DWT_Delay_Init();
	Set_Pin_Output (DHT11_PORT, DHT11_PIN);  // set the pin as output
	HAL_GPIO_WritePin (DHT11_PORT, DHT11_PIN, 0);   // pull the pin low
	delay(18000);   // wait for 18ms
    HAL_GPIO_WritePin (DHT11_PORT, DHT11_PIN, 1);   // pull the pin high
    delay(20);
	Set_Pin_Input(DHT11_PORT, DHT11_PIN);    // set as input
}

uint8_t DHT11_Check_Response (void) //comprueba que se ha inicializado correctamente
{
	uint8_t Response = 0;
	delay(40);
	if (!(HAL_GPIO_ReadPin (DHT11_PORT, DHT11_PIN)))
	{
		delay(80);
		if ((HAL_GPIO_ReadPin (DHT11_PORT, DHT11_PIN)))
			Response = 1;
		else
			Response = -1;
	}
	uint32_t tickstart = HAL_GetTick();
	while ((HAL_GPIO_ReadPin (DHT11_PORT, DHT11_PIN)))   // wait for the pin to go low
	{
		if(HAL_GetTick() - tickstart > 1) //antibloqueo
			break;
	}
	return Response;
}

uint8_t DHT11_Read (void) //se realiza la lectura de un byte
{
	uint8_t i,j;
	for (j=0;j<8;j++) //little endian (7 - 6 - 5 - 4 - 3 - 2 - 1 - 0)
	{
		uint32_t tickstart = HAL_GetTick();
		while (!(HAL_GPIO_ReadPin (DHT11_PORT, DHT11_PIN)))   // wait for the pin to go high
		{
			if(HAL_GetTick() - tickstart > 1) //antibloqueo
				break;
		}
		delay(40);   // wait for 40 us
		if (!(HAL_GPIO_ReadPin (DHT11_PORT, DHT11_PIN)))   // if the pin is low
			i&= ~(1<<(7-j));   // write 0 (AND con 0 del bit 7-j)
		else
			i|= (1<<(7-j));  // if the pin is high, write 1 (OR con 1 del bit 7-j)
		tickstart = HAL_GetTick();
		while ((HAL_GPIO_ReadPin (DHT11_PORT, DHT11_PIN)))  // wait for the pin to go low
		{
			if(HAL_GetTick() - tickstart > 1) //antibloqueo
				break;
		}
	}
	return i;
}

void DHT11_GetData (DHT11_DataTypedef *DHT11_Data)
{
	DWT_Delay_Init();
    DHT11_Start();
	Presence = DHT11_Check_Response();
	Rh_byte1 = DHT11_Read();
	Rh_byte2 = DHT11_Read();
	Temp_byte1 = DHT11_Read();
	Temp_byte2 = DHT11_Read();
	SUM = DHT11_Read();

	if (SUM == (Rh_byte1 + Rh_byte2 + Temp_byte1 + Temp_byte2)) //se comprueba que la lectura es correcta (checksum)
	{
		DHT11_Data->Temperature_int = Temp_byte1;
		DHT11_Data->Temperature_dec = Temp_byte2;
		DHT11_Data->Humidity_int = Rh_byte1;
		DHT11_Data->Humidity_dec = Rh_byte2;
	}
}
