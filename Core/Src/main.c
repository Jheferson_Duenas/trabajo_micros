/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

#include "hcsr04.h"
#include "dht11.h"
#include "ssd1306.h"
#include "fonts.h"
#include "ir_controller.h"
#include "general.h"
#include "i2c-lcd.h"
#include <stdio.h>
#include <string.h>

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc1;
DMA_HandleTypeDef hdma_adc1;

I2C_HandleTypeDef hi2c1;
I2C_HandleTypeDef hi2c2;

TIM_HandleTypeDef htim1;
TIM_HandleTypeDef htim2;
TIM_HandleTypeDef htim3;
TIM_HandleTypeDef htim4;
TIM_HandleTypeDef htim5;

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_TIM1_Init(void);
static void MX_I2C1_Init(void);
static void MX_TIM2_Init(void);
static void MX_TIM3_Init(void);
static void MX_TIM4_Init(void);
static void MX_TIM5_Init(void);
static void MX_ADC1_Init(void);
static void MX_I2C2_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

//Clave
char code, clave_in[10];

//Distancia
uint32_t volatile distancia;

//Temperatura/Humedad
DHT11_DataTypedef DHT11_Data;
uint8_t volatile temperatura_int, temperatura_dec, humedad_int, humedad_dec;
float volatile temperatura, humedad;

//Contador de puerta abierta
uint8_t volatile contador_puerta = 0;

//Rango
uint32_t rango[2];
uint32_t volatile rango_temperatura, rango_humedad;
uint8_t volatile lim_temperatura_inf, lim_temperatura_sup, lim_humedad_inf, lim_humedad_sup;

//Flags
uint8_t volatile puerta = 0, pulsacion = 0, boton = 0, flag_200ms = 0, flag_3s = 0, flag_1s = 0;

//Funciones

uint8_t clave(char* clave)
{
	SSD1306_GotoXY(0, 0);
	SSD1306_Puts("Introducir clave: ", &Font_7x10, 1);
	SSD1306_UpdateScreen();
	for(int i = 0; i < strlen(clave); i++) //se realizan tantas lecturas como caracteres tenga la clave
	{
		do {
			code = get_code();
		} while(code == 'x'); //bucle para filtrar lecturas no válidas

		clave_in[i] = code;
		SSD1306_GotoXY(20 + 10 * i, 20); //se escribe el caracter introducido en la pantalla OLED
		SSD1306_Putc(code, &Font_7x10, 1);
		SSD1306_UpdateScreen();
	}

	if(strcmp(clave_in, clave) == 0) //se compara la clave introducida con la clave
	{
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_3, 1); //doble pitido
		HAL_Delay(100);
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_3, 0);
		HAL_Delay(100);
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_3, 1);
		HAL_Delay(100);
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_3, 0);
		SSD1306_Clear();
		SSD1306_GotoXY(10, 20);
		SSD1306_Puts("Clave correcta", &Font_7x10, 1);
		SSD1306_UpdateScreen();
		HAL_Delay(500);
		SSD1306_Clear();
		return 1; //clave correcta
	}
	else
	{
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_3, 1); //pitido simple
		HAL_Delay(200);
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_3, 0);
		SSD1306_Clear();
		SSD1306_GotoXY(10, 20);
		SSD1306_Puts("Clave incorrecta", &Font_7x10, 1);
		SSD1306_UpdateScreen();
		HAL_Delay(500);
		SSD1306_Clear();
		return 0; //clave incorrecta
	}
}

void lectura_distancia(void)
{
	if(flag_200ms == 1)
	{
		uint8_t distancia_0 = HCSR04_GetDistance(); //lectura de distancia

		if(distancia_0 < 20 && distancia_0 > 3) //restricciones impuestas por la maqueta y el rango de medida del sensor
			distancia = distancia_0;

		flag_200ms = 0;
	}
}

void lectura_temperatura_humedad(void)
{
	if(flag_3s == 1)
	{
		HAL_ADC_Stop_DMA(&hadc1); //durante la lectura se pausa ADC
		DHT11_GetData(&DHT11_Data); //lectura de temperatura y humedad
		HAL_ADC_Start_DMA(&hadc1, (uint32_t *) rango, 2);

		temperatura_int = DHT11_Data.Temperature_int;
		temperatura_dec = DHT11_Data.Temperature_dec;
		humedad_int = DHT11_Data.Humidity_int;
		humedad_dec = DHT11_Data.Humidity_dec;

		temperatura = temperatura_int + 0.1 * temperatura_dec;
		humedad = humedad_int + 0.1 * humedad_dec;

		flag_3s = 0;
	}
}

void estado_puerta(void)
{
	if(puerta == 0) //si la puerta está cerrada
	{
		if(distancia < 11 && distancia != 0) //la puerta se ha abierto y no es la primera iteración
		{
			lcd_clear();
			uint32_t tickstart = HAL_GetTick();
			while(HAL_GetTick() - tickstart < 2000) //mensaje durante 2 s
			{
				lcd_send_string("PUERTA");
				lcd_put_cur(1, 0);
				lcd_send_string("ABIERTA");
				lcd_put_cur(0, 0);
			}
			lcd_clear();
			puerta = 1; //puerta abierta
		}
	}
	else //si la puerta está abierta
	{
		if(distancia > 11) //la puerta se ha cerrado
		{
			lcd_clear();
			uint32_t tickstart = HAL_GetTick();
			while(HAL_GetTick() - tickstart < 2000) //mensaje durante 2 s
			{
				lcd_send_string("PUERTA");
				lcd_put_cur(1, 0);
				lcd_send_string("CERRADA");
				lcd_put_cur(0, 0);
			}
			lcd_clear();
		 	puerta = 0; //puerta cerrada
		}
	}
}

int debouncer(volatile uint8_t* button_int, GPIO_TypeDef* GPIO_Port, uint16_t GPIO_Pin)
{
	static uint8_t button_count = 0;
	static int counter = 0;

	if(*button_int == 1)
	{
		if(button_count == 0)
		{
			counter = HAL_GetTick();
			button_count++;
		}
		if(HAL_GetTick() - counter >= 20)
		{
			counter = HAL_GetTick();
			if(HAL_GPIO_ReadPin(GPIO_Port, GPIO_Pin) != 1)
				button_count = 1;
			else
				button_count ++;
			if(button_count == 4) //periodo antirrebotes
			{
				button_count = 0;
				*button_int = 0;
				return 1;
			}
		}
	}
	return 0;
}

void pulsador_puerta(void)
{
	if(debouncer(&boton, GPIOA, GPIO_PIN_0) == 1)
	{
		if(pulsacion == 1)
		{
			__HAL_TIM_SET_COMPARE(&htim4, TIM_CHANNEL_1, 50); //puerta cerrada
			pulsacion = 0;
		}
		else
		{
			 __HAL_TIM_SET_COMPARE(&htim4, TIM_CHANNEL_1, 105); //puerta abierta
			 pulsacion = 1;
		}
	}
}

void aviso_puerta_abierta(void)
{
	if(contador_puerta >= 10) //han pasado 10s con la puerta abierta
	{
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_3, 1); //pitido
		HAL_Delay(300);
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_3, 0);
		__HAL_TIM_SET_COMPARE(&htim4, TIM_CHANNEL_1, 50); //se cierra la puerta
		pulsacion = 0; //reinicio de pulsacion
		contador_puerta = 0; //reinicio del contador
	}
}

void lim_temperatura_humedad(uint8_t lim_temp_inf, uint8_t lim_temp_sup, uint8_t paso_temp, uint8_t lim_hum_inf, uint8_t lim_hum_sup, uint8_t paso_hum)
{
	uint16_t resolucion = 1024; //resolución de ADC1: 10 bits -> 1024 enteros [0 - 1024]
	static uint8_t lim_temp_0 = 0, lim_hum_0 = 0; //límite de la iteración anterior
	float factor_hum = resolucion / (lim_hum_sup - lim_hum_inf); //traducción de los valores del potenciómetro a los límites
	float factor_temp = resolucion / (lim_temp_sup - lim_temp_inf);

	lim_temperatura_inf = lim_temp_inf + rango_temperatura / factor_temp; //límite superior
	lim_temperatura_sup = lim_temperatura_inf + paso_temp; //límite inferior

	lim_humedad_inf = lim_hum_inf + rango_humedad / factor_hum;
	lim_humedad_sup = lim_humedad_inf + paso_hum;

	if((lim_temperatura_inf != lim_temp_0 || lim_humedad_inf != lim_hum_0) && (lim_temp_0 != 0 && lim_hum_0 != 0)) //si el rango ha cambiado respecto a la anterior iteración, se actualiza la pantalla
	{																											   //y si no es la primera iteración
		char cad[30];
		SSD1306_Clear();

		uint32_t tickstart = HAL_GetTick();
		while(HAL_GetTick() - tickstart < 3000) //la pantalla cambia durante 3 segundos
		{
			lim_temperatura_inf = lim_temp_inf + rango_temperatura / factor_temp;
			lim_temperatura_sup = lim_temperatura_inf + paso_temp;
			lim_humedad_inf = lim_hum_inf + rango_humedad / factor_hum;
			lim_humedad_sup = lim_humedad_inf + paso_hum;

			SSD1306_GotoXY(0, 10);
			SSD1306_Puts("Temperatura", &Font_7x10, 1);
			SSD1306_GotoXY(0, 20);
			sprintf(cad, "[%d - %d]C", lim_temperatura_inf, lim_temperatura_sup);
			SSD1306_Puts(cad, &Font_7x10, 1);
			SSD1306_GotoXY(0, 40);
			SSD1306_Puts("Humedad", &Font_7x10, 1);
			SSD1306_GotoXY(0, 50);
			sprintf(cad, "[%d - %d]%%", lim_humedad_inf, lim_humedad_sup);
			SSD1306_Puts(cad, &Font_7x10, 1);
			SSD1306_UpdateScreen();
		}
		SSD1306_Clear();
	}

	lim_temp_0 = lim_temperatura_inf; //actualización del límite de la iteración anterior
	lim_hum_0 = lim_humedad_inf;
}

void estado_temperatura_humedad(void)
{
	char cad[30];
	if(temperatura != 0)
	{
		sprintf(cad, "Temperatura: %d.%dC", temperatura_int, temperatura_dec);
		SSD1306_GotoXY(0, 0);
		SSD1306_Puts(cad, &Font_7x10, 1);

		sprintf(cad, "Humedad: %d.%d%%", humedad_int, humedad_dec);
		SSD1306_GotoXY(0, 20);
		SSD1306_Puts(cad, &Font_7x10, 1);

		if(temperatura < lim_temperatura_inf) //si la temperatura no está en el rango aparece mensaje en la pantalla OLED
		{
			SSD1306_GotoXY(0, 40);
			SSD1306_Puts("Temperatura baja!", &Font_7x10, 1);
		}

		else if(temperatura > lim_temperatura_sup)
		{
			SSD1306_GotoXY(0, 40);
			SSD1306_Puts("Temperatura alta!", &Font_7x10, 1);
		}

		else
		{
			SSD1306_GotoXY(0, 40);
			SSD1306_Puts("                 ", &Font_7x10, 1);
		}

		if(humedad < lim_humedad_inf) //si la humedad no está en el rango aparece mensaje en la pantalla OLED
		{
			SSD1306_GotoXY(0, 50);
			SSD1306_Puts("Humedad baja!", &Font_7x10, 1);
		}

		else if(humedad > lim_humedad_sup)
		{
			SSD1306_GotoXY(0, 50);
			SSD1306_Puts("Humedad alta!", &Font_7x10, 1);
		}

		else
		{
			SSD1306_GotoXY(0, 50);
			SSD1306_Puts("             ", &Font_7x10, 1);
		}
		SSD1306_UpdateScreen();
	}
}

void HAL_TIM_IC_CaptureCallback(TIM_HandleTypeDef *htim)
{
	HCSR04_CaptureCallback(htim);
}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef* htim)
{
	if (htim->Instance == TIM2) //200 ms
		flag_200ms = 1;
	if(htim->Instance == TIM3) //3 s
		flag_3s = 1;
	if (htim->Instance == TIM5) //1 s
	{
		if(puerta == 1) //si la puerta está abierta
			contador_puerta++; //contador de segundos con la puerta abierta
		else //si la puerta está cerrada
			contador_puerta = 0;
	}
}

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	if(GPIO_Pin == GPIO_PIN_0)
		boton = 1;
}

void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc)
{
	rango_temperatura = rango[0];
	rango_humedad = rango[1];
}

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_TIM1_Init();
  MX_I2C1_Init();
  MX_TIM2_Init();
  MX_TIM3_Init();
  MX_TIM4_Init();
  MX_TIM5_Init();
  MX_ADC1_Init();
  MX_I2C2_Init();
  /* USER CODE BEGIN 2 */

  HAL_TIM_IC_Start(&htim1, TIM_CHANNEL_1); //ultrasonidos
  HAL_TIM_Base_Start_IT(&htim2); //lectura cada 200 ms
  HAL_TIM_Base_Start_IT(&htim3); //lectura cada 3 s
  HAL_TIM_PWM_Start(&htim4, TIM_CHANNEL_1); //servo
  HAL_TIM_Base_Start_IT(&htim5); //lectura cada 1 s

  HAL_ADC_Start_DMA(&hadc1, (uint32_t *) rango, 2); //potenciómetros

  SSD1306_Init(); //oled
  lcd_init(); //lcd

  __HAL_TIM_SET_COMPARE(&htim4, TIM_CHANNEL_1, 50); //inicialización de servo a 0º (puerta cerrada)
  while(clave("123") == 0); //clave para entrar al sistema

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */

	  lectura_distancia();
	  lectura_temperatura_humedad();

	  estado_puerta();
	  pulsador_puerta();
	  aviso_puerta_abierta();

	  lim_temperatura_humedad(15, 30, 4, 30, 60, 9);
	  estado_temperatura_humedad();
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief ADC1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_ADC1_Init(void)
{

  /* USER CODE BEGIN ADC1_Init 0 */

  /* USER CODE END ADC1_Init 0 */

  ADC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN ADC1_Init 1 */

  /* USER CODE END ADC1_Init 1 */
  /** Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion)
  */
  hadc1.Instance = ADC1;
  hadc1.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV2;
  hadc1.Init.Resolution = ADC_RESOLUTION_10B;
  hadc1.Init.ScanConvMode = ENABLE;
  hadc1.Init.ContinuousConvMode = ENABLE;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc1.Init.NbrOfConversion = 2;
  hadc1.Init.DMAContinuousRequests = ENABLE;
  hadc1.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
  if (HAL_ADC_Init(&hadc1) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time.
  */
  sConfig.Channel = ADC_CHANNEL_4;
  sConfig.Rank = 1;
  sConfig.SamplingTime = ADC_SAMPLETIME_480CYCLES;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time.
  */
  sConfig.Channel = ADC_CHANNEL_5;
  sConfig.Rank = 2;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN ADC1_Init 2 */

  /* USER CODE END ADC1_Init 2 */

}

/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C1_Init(void)
{

  /* USER CODE BEGIN I2C1_Init 0 */

  /* USER CODE END I2C1_Init 0 */

  /* USER CODE BEGIN I2C1_Init 1 */

  /* USER CODE END I2C1_Init 1 */
  hi2c1.Instance = I2C1;
  hi2c1.Init.ClockSpeed = 400000;
  hi2c1.Init.DutyCycle = I2C_DUTYCYCLE_2;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C1_Init 2 */

  /* USER CODE END I2C1_Init 2 */

}

/**
  * @brief I2C2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C2_Init(void)
{

  /* USER CODE BEGIN I2C2_Init 0 */

  /* USER CODE END I2C2_Init 0 */

  /* USER CODE BEGIN I2C2_Init 1 */

  /* USER CODE END I2C2_Init 1 */
  hi2c2.Instance = I2C2;
  hi2c2.Init.ClockSpeed = 100000;
  hi2c2.Init.DutyCycle = I2C_DUTYCYCLE_2;
  hi2c2.Init.OwnAddress1 = 0;
  hi2c2.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c2.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c2.Init.OwnAddress2 = 0;
  hi2c2.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c2.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C2_Init 2 */

  /* USER CODE END I2C2_Init 2 */

}

/**
  * @brief TIM1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM1_Init(void)
{

  /* USER CODE BEGIN TIM1_Init 0 */

  /* USER CODE END TIM1_Init 0 */

  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_IC_InitTypeDef sConfigIC = {0};

  /* USER CODE BEGIN TIM1_Init 1 */

  /* USER CODE END TIM1_Init 1 */
  htim1.Instance = TIM1;
  htim1.Init.Prescaler = 16-1;
  htim1.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim1.Init.Period = 0xffff-1;
  htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim1.Init.RepetitionCounter = 0;
  htim1.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_IC_Init(&htim1) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigIC.ICPolarity = TIM_INPUTCHANNELPOLARITY_RISING;
  sConfigIC.ICSelection = TIM_ICSELECTION_DIRECTTI;
  sConfigIC.ICPrescaler = TIM_ICPSC_DIV1;
  sConfigIC.ICFilter = 0;
  if (HAL_TIM_IC_ConfigChannel(&htim1, &sConfigIC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM1_Init 2 */

  /* USER CODE END TIM1_Init 2 */

}

/**
  * @brief TIM2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM2_Init(void)
{

  /* USER CODE BEGIN TIM2_Init 0 */

  /* USER CODE END TIM2_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM2_Init 1 */

  /* USER CODE END TIM2_Init 1 */
  htim2.Instance = TIM2;
  htim2.Init.Prescaler = 2000-1;
  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim2.Init.Period = 1600-1;
  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim2) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim2, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM2_Init 2 */

  /* USER CODE END TIM2_Init 2 */

}

/**
  * @brief TIM3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM3_Init(void)
{

  /* USER CODE BEGIN TIM3_Init 0 */

  /* USER CODE END TIM3_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM3_Init 1 */

  /* USER CODE END TIM3_Init 1 */
  htim3.Instance = TIM3;
  htim3.Init.Prescaler = 10000-1;
  htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim3.Init.Period = 4800-1;
  htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim3.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim3) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim3, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim3, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM3_Init 2 */

  /* USER CODE END TIM3_Init 2 */

}

/**
  * @brief TIM4 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM4_Init(void)
{

  /* USER CODE BEGIN TIM4_Init 0 */

  /* USER CODE END TIM4_Init 0 */

  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};

  /* USER CODE BEGIN TIM4_Init 1 */

  /* USER CODE END TIM4_Init 1 */
  htim4.Instance = TIM4;
  htim4.Init.Prescaler = 320-1;
  htim4.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim4.Init.Period = 1000-1;
  htim4.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim4.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_PWM_Init(&htim4) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim4, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 500;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_PWM_ConfigChannel(&htim4, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM4_Init 2 */

  /* USER CODE END TIM4_Init 2 */
  HAL_TIM_MspPostInit(&htim4);

}

/**
  * @brief TIM5 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM5_Init(void)
{

  /* USER CODE BEGIN TIM5_Init 0 */

  /* USER CODE END TIM5_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM5_Init 1 */

  /* USER CODE END TIM5_Init 1 */
  htim5.Instance = TIM5;
  htim5.Init.Prescaler = 5000-1;
  htim5.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim5.Init.Period = 3200-1;
  htim5.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim5.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim5) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim5, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim5, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM5_Init 2 */

  /* USER CODE END TIM5_Init 2 */

}

/**
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void)
{

  /* DMA controller clock enable */
  __HAL_RCC_DMA2_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA2_Stream0_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA2_Stream0_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA2_Stream0_IRQn);

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOE_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_1|GPIO_PIN_3, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOE, GPIO_PIN_10, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOD, GPIO_PIN_15, GPIO_PIN_RESET);

  /*Configure GPIO pin : PA0 */
  GPIO_InitStruct.Pin = GPIO_PIN_0;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : PA1 PA3 */
  GPIO_InitStruct.Pin = GPIO_PIN_1|GPIO_PIN_3;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin : PA2 */
  GPIO_InitStruct.Pin = GPIO_PIN_2;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin : PE10 */
  GPIO_InitStruct.Pin = GPIO_PIN_10;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

  /*Configure GPIO pin : PD15 */
  GPIO_InitStruct.Pin = GPIO_PIN_15;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI0_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI0_IRQn);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
