/*
 * hcsr04.c
 *
 *  Created on: Dec 8, 2020
 *      Author: Andrea
 *
 *	HC-SR04
 */

#include "hcsr04.h"
#include "general.h"
#include <stdlib.h>

uint32_t IC_Val1 = 0;
uint32_t IC_Val2 = 0;
uint32_t Difference = 0;
uint8_t Is_First_Captured = 0;  // is the first value captured ?
uint32_t Distance = 0;

void HCSR04_CaptureCallback(TIM_HandleTypeDef *htim)
{
	if (htim->Channel == CHANNEL_IT)  // if the interrupt source is channel1
	{
		if (Is_First_Captured==0) // if the first value is not captured
		{
			IC_Val1 = HAL_TIM_ReadCapturedValue(htim, CHANNEL); // read the first value
			Is_First_Captured = 1;  // set the first captured as true
			// Now change the polarity to falling edge
			__HAL_TIM_SET_CAPTUREPOLARITY(htim, CHANNEL, TIM_INPUTCHANNELPOLARITY_FALLING);
		}

		else if (Is_First_Captured==1)   // if the first is already captured
		{
			IC_Val2 = HAL_TIM_ReadCapturedValue(htim, CHANNEL);  // read second value
			__HAL_TIM_SET_COUNTER(htim, 0);  // reset the counter

			if (IC_Val2 > IC_Val1)
			{
				Difference = IC_Val2-IC_Val1;
			}

			else if (IC_Val1 > IC_Val2)
			{
				Difference = (0xffff - IC_Val1) + IC_Val2;
			}

			Distance = Difference * 0.034/2; //velocidad del sonido: 0.034 cm/us, se divide entre 2 porque calcula ida y vuelta
			Is_First_Captured = 0; // set it back to false

			// set polarity to rising edge
			__HAL_TIM_SET_CAPTUREPOLARITY(htim, CHANNEL, TIM_INPUTCHANNELPOLARITY_RISING);
			__HAL_TIM_DISABLE_IT(&timer, TIM_IT_CC1);
		}
	}
}

void HCSR04_Read (void)
{
	DWT_Delay_Init();
	HAL_GPIO_WritePin(TRIG_PORT, TRIG_PIN, GPIO_PIN_SET);  // pull the TRIG pin HIGH
	delay(10);  // wait for 10 us
	HAL_GPIO_WritePin(TRIG_PORT, TRIG_PIN, GPIO_PIN_RESET);  // pull the TRIG pin low

	__HAL_TIM_ENABLE_IT(&timer, TIM_IT_CC1);
}

uint32_t HCSR04_GetDistance(void)
{
	HCSR04_Read();
	return Distance;
}
