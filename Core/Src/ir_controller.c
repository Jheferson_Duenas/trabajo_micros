/*
 * ir_controller.c
 *
 *  Created on: Dec 29, 2020
 *      Author: Andrea
 *
 *  IR Receiver
 */

#include "stm32f4xx_hal.h"
#include "ir_controller.h"
#include "general.h"
#include <string.h>

uint8_t count = 0; //contador de ms
uint32_t data;
char password_in[10];

uint32_t receive_data (void)
{
	uint32_t code = 0;
	DWT_Delay_Init();

		  /* The START Sequence begin here
	   * there will be a pulse of 9ms LOW and
	   * than 4.5 ms space (HIGH)
	   */
	  while (!(HAL_GPIO_ReadPin(IR_CONTROLLER_PORT, IR_CONTROLLER_PIN)));  // wait for the pin to go high.. 9ms LOW

	  while ((HAL_GPIO_ReadPin(IR_CONTROLLER_PORT, IR_CONTROLLER_PIN)));  // wait for the pin to go low.. 4.5ms HIGH

	  /* START of FRAME ends here*/

	  /* DATA Reception
	   * We are only going to check the SPACE after 562.5us pulse
	   * if the space is 562.5us, the bit indicates '0'
	   * if the space is around 1.6ms, the bit is '1'
	   */

	  for (int i = 0; i < 32; i++) //little endian (31 - 30 - 29 - ... - 2 - 1 - 0)
	  {
		  count = 0;

		  while (!(HAL_GPIO_ReadPin(IR_CONTROLLER_PORT, IR_CONTROLLER_PIN))); // wait for pin to go high.. this is 562.5us LOW

		  while ((HAL_GPIO_ReadPin(IR_CONTROLLER_PORT, IR_CONTROLLER_PIN)))  // count the space length while the pin is high
		  {
			  count++;
			  delay(100);
		  }
		  if (count > 12) // if the space is more than 1.2 ms
		  {
			  code |= (1UL << (31-i));   // write 1 (OR con 1 del bit 31-i)
		  }
		  else code &= ~(1UL << (31-i));  // write 0 (AND con 0 del bit 31-i)
	  }
		return code;
}

char convert_code (uint32_t code)
{
	switch (code)
	{
		case (0xFF6897):
			return('0');
			break;
		case (0xFF30CF):
			return('1');
			break;
		case (0xFF18E7):
			return('2');
			break;
		case (0xFF7A85):
			return('3');
			break;
		case (0xFF10EF):
			return('4');
			break;
		case (0xFF38C7):
			return('5');
			break;
		case (0xFF5AA5):
			return('6');
			break;
		case (0xFF42BD):
			return('7');
			break;
		case (0xFF4AB5):
			return('8');
			break;
		case (0xFF52AD):
			return('9');
			break;
		default :
			return('x');
			break;
	}
}

char get_code(void)
{
	while (HAL_GPIO_ReadPin(IR_CONTROLLER_PORT, IR_CONTROLLER_PIN)); //se espera valor bajo del pin de señal
	uint32_t data = receive_data(); //se reciben los datos
	char code = convert_code(data); //se decodifican los datos
	HAL_Delay(200); //delay entre lecturas
	return code;
}
