/*
 * hcsr04.h
 *
 *  Created on: Dec 8, 2020
 *      Author: Andrea
 *
 *  Adaptación de las funciones de https://controllerstech.com/
 *
 *	HC-SR04
 *		- 4 pines:
 *			- Vcc: 5 V
 *			- Trigg: GPIO Output
 *			- Echo: TIM (Input Capture)
 *			- GND
 */

#ifndef INC_HCSR04_H_
#define INC_HCSR04_H_

#include "stm32f4xx_hal.h"

//////////////////////////////////////////////////////////////////////////////////////////

#define TRIG_PIN GPIO_PIN_10 //pin del trigger
#define TRIG_PORT GPIOE //puerto del trigger

#define timer htim1 //timer
extern TIM_HandleTypeDef timer;

#define CHANNEL_IT HAL_TIM_ACTIVE_CHANNEL_1 //canal del timer
#define CHANNEL TIM_CHANNEL_1

//////////////////////////////////////////////////////////////////////////////////////////

void HCSR04_CaptureCallback(TIM_HandleTypeDef *htim); //incluir en el callback del timer
uint32_t HCSR04_GetDistance(void); //devuelve la distancia

#endif /* INC_HCSR04_H_ */
