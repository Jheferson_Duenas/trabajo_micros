/*
 * ir_controller.h
 *
 *  Created on: Dec 29, 2020
 *      Author: Andrea
 *
 *  Adaptación de las funciones de https://controllerstech.com/
 *
 *  IR Receiver:
 *  	- 3 pines:
 *  		- Izquierda: señal (GPIO Input)
 *  		- Centro: GND
 *  		- Derecha: 5 V
 */

#ifndef INC_IR_CONTROLLER_H_
#define INC_IR_CONTROLLER_H_

////////////////////////////////////////////////////////////////////////////

#define IR_CONTROLLER_PORT GPIOA //puerto de datos
#define IR_CONTROLLER_PIN GPIO_PIN_2 //pin de datos

////////////////////////////////////////////////////////////////////////////

char get_code(void); //devuelve el dato codificado

#endif /* INC_IR_CONTROLLER_H_ */
