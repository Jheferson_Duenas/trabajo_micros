/*
 * dht11.h
 *
 *  Created on: Dec 8, 2020
 *      Author: Andrea
 *
 *  Adaptación de las funciones de https://controllerstech.com/
 *
 *  DHT11
 *  	- 3 pines:
 *  		- SIGNAL: GPIO Output
 *  		- VCC: 5 V
 *  		- GND
 */

#ifndef INC_DHT11_H_
#define INC_DHT11_H_

/////////////////////////////////////////////////////////////////////////////////////

#define DHT11_PORT GPIOA //puerto de datos
#define DHT11_PIN GPIO_PIN_1 //pin de datos

/////////////////////////////////////////////////////////////////////////////////////

typedef struct
{
	uint8_t Temperature_int; //temperatura (parte entera)
	uint8_t Temperature_dec; //temperatura (parte decimal)
	uint8_t Humidity_int; //humedad (parte entera)
	uint8_t Humidity_dec; //humedad (parte decimal)
}DHT11_DataTypedef;

void DHT11_GetData (DHT11_DataTypedef *DHT11_Data); //devuelve humedad y temperatura por referencia

#endif /* INC_DHT11_H_ */
