/*
 * general.h
 *
 *  Created on: Dec 8, 2020
 *      Author: Andrea
 *
 *	Funciones de ámbito general
 */

#ifndef INC_GENERAL_H_
#define INC_GENERAL_H_

#include "stm32f4xx_hal.h"

uint32_t DWT_Delay_Init(void); //inicialización de delay en microsegundos
void delay(volatile uint32_t microseconds); //delay en microsegundos

void Set_Pin_Output(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin); //definir pin como salida
void Set_Pin_Input(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin); //definir pin como entrada

#endif /* INC_GENERAL_H_ */
